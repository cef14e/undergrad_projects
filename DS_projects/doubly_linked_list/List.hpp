#include"List.h"

/**
 * This file contains definitions for the class List.h.
 * 
 * Camill Folsom
 * June 15, 2017
 */

/*****************************************************************************/
/************** Beginning of const_iterator class definitions ****************/
/*****************************************************************************/

/**
 * @name (Default) Constructor - const_iterator
 * @brief Sets pointer current to NULL.
 */
template <typename T>
List<T>::const_iterator::const_iterator() : current{nullptr} {};

/** 
 * @name Operator*() - const_iterator
 * @brief Returns a reference to the corresponding element in the list by 
 *        calling retrieve() member function
 */
template <typename T>
const T & List<T>::const_iterator::operator*() const {return retrieve();}

/**
 * @name Operator++() - const_iterator
 * @brief Prefix increment operator.
 */
template <typename T> 
typename List<T>::const_iterator::const_iterator& List<T>::const_iterator::operator++()
{
	current = current -> next;
	return *this;
}

/**
 * @name Operator++(int) - const_iterator
 * @brief Postfix increment operator.
 */
template <typename T>
typename List<T>::const_iterator::const_iterator List<T>::const_iterator::operator++(int)
{
	const_iterator old = *this;
	++(*this);
	return old;
}

/**
 * @name Operator--() - const_iterator
 * @brief Prefix decrement operator.
 */
template <typename T>
typename List<T>::const_iterator::const_iterator & List<T>::const_iterator::operator--()
{
	current = current -> prev;
	return *this;
}

/**
 * @name Operator--(int) - const_iterator
 * @brief Postfix decrement operator.
 */
template <typename T>
typename List<T>::const_iterator::const_iterator List<T>::const_iterator::operator--(int)
{
	const_iterator old = *this;
	--(*this);
	return old;
}

/**
 * @name Operator==() Equality Operator - const_iterator
 * @brief Two iterator are equal if they refer to the same element.
 */
template <typename T>
bool List<T>::const_iterator::operator==(const const_iterator &rhs) const
{return current == rhs.current;}

/**
 * @name Operator!=() Non-Equality Operator - const_iterator
 * @brief Two iterator are not equal if they do not refer to the same element.
 */
template <typename T>
bool List<T>::const_iterator::operator!=(const const_iterator &rhs) const
{return current != rhs.current;}

/**
 * @name Retrieve
 * @brief Returns a reference to the corresponding element in the list.
 */
template <typename T>
T & List<T>::const_iterator::retrieve() const {return current -> data;}

/**
 * @name (Parameter) Constructor - const_iterator
 * @brief Sets pointer current to the given node pointer p.
 */
template <typename T>
List<T>::const_iterator::const_iterator(Node *p) : current{p} {};

/*****************************************************************************/
/************** Beginning of iterator class definitions **********************/
/*****************************************************************************/

/**
 * @name (Default) Constructor - Iterator
 * @brief Uses const_iterators default constructor.
 */
template <typename T>
List<T>::iterator::iterator() = default;

/**
 * @name Operator*() - Iterator
 * @brief Returns a reference to the corresponding element in the list by 
 *        calling const_iterators retrieve() member function. 
 */
template <typename T>
T & List<T>::iterator::operator*()
{return const_iterator::retrieve();}

/**
 * @name Constant Operator*() - Iterator
 * @brief Returns a reference to the corresponding element in the list by 
 *        calling const_iterators operator*() member function.
 */
template <typename T>
const T & List<T>::iterator::operator*() const
{return const_iterator::operator*();}

/**
 * @name Operator++() - Iterator
 * @brief Prefix increment operator.
 */
template <typename T> 
typename List<T>::iterator::iterator& List<T>::iterator::operator++()
{
	this -> current = this -> current -> next;
	return *this;
}

/**
 * @name Operator++(int) - Iterator
 * @brief Postfix increment operator.
 */
template <typename T>
typename List<T>::iterator::iterator List<T>::iterator::operator++(int)
{
	iterator old = *this;
	++(*this);
	return old;
}

/**
 * @name Operator--() - Iterator
 * @brief Prefix decrement operator.
 */
template <typename T>
typename List<T>::iterator::iterator & List<T>::iterator::operator--()
{
	this -> current = this -> current -> prev;
	return *this;
}

/**
 * @name Operator--(int) - Iterator
 * @brief Postfix decrement operator.
 */
template <typename T>
typename List<T>::iterator::iterator List<T>::iterator::operator--(int)
{
	iterator old = *this;
	--(*this);
	return old;
}

/**
 * @name (Parameter) Constructor - Iterator
 * @brief Uses const_iterators Parameter constructor to set pointer current to 
 *        the given node pointer p
 */
template <typename T>
List<T>::iterator::iterator(Node *p) : List<T>::const_iterator{p} {};

/*****************************************************************************/
/************** Beginning of List Class definitions  *************************/
/*****************************************************************************/

/**
 * @name init()
 * @brief Initialize the member variables of list.
 */
template <typename T>
void List<T>::init()
{
	theSize = 0;
	head = new Node;
	tail = new Node;
	head -> next = tail;
	tail -> prev = head;
}

/**
 * @name (Default) Constructor - List
 * @brief Call init() to initialize list member variables
 */
template <typename T>
List<T>::List() {init();};

/**
 * @name Destructor 
 */
template <typename T>
List<T>::~List()
{
	clear();
	delete head;
	delete tail;
}

/**
 * @name Copy Constructor 
 */
template <typename T>
List<T>::List(const List &rhs)
{
	init();
	for(auto &x : rhs)
		push_back(x);
}

/**
 * @name Move Constructor 
 */
template <typename T>
List<T>::List(List && rhs):theSize{rhs.theSize}, head{rhs.head}, tail{rhs.tail}
{
	rhs.theSize = 0;
	rhs.head = nullptr;
	rhs.tail = nullptr;
}

/**
 * @name (Parameter) Constructor - List
 * @brief Construct List with num elements, all initialized to val.
 */
template <typename T>
List<T>::List(int num, const T& val)
{
	init();
	
	for(int i = 0; i < num; ++i)
		insert(begin(), val);
}

/** 
 * @name Copy (Parameter) Constructor
 * @brief Construct List with elements from another list [star,end) iterators.
 */
template <typename T>
List<T>::List(const_iterator start, const_iterator end)
{
	init();
	for(const_iterator itr = start; itr != end; ++itr)
		push_back(*itr);
}

/**
 * @name (Initializer List) Constructor
 * @brief Construct List using a initializer list
 */
template <typename T>
List<T>::List(std::initializer_list<T> iList)
{
	init();
	for(auto itr = iList.begin(); itr != iList.end(); ++itr)
		push_back(*itr);
}

/** 
 * @name Copy Assignment Operator=
 */
template <typename T>
const List<T>& List<T>::operator=(const List<T> &rhs)
{
	List copy = rhs;
	std::swap(*this, copy);
	return *this;
}

/**
 * @name Move Assignment Operator 
 */
template <typename T>
List<T>& List<T>::operator=(List &&rhs)
{
	std::swap(theSize, rhs.theSize);
	std::swap(head, rhs.head);
	std::swap(tail, rhs.tail);
	return *this;
}

/**
 * @name Initializer List Assignment Operator=
 * @brief Assign the initializer list data to be the calling object's new data.
 */
template <typename T>
List<T>& List<T>::operator=(std::initializer_list<T> iList) 
{
	List copy = iList;
	std::swap(*this, copy);
	return *this;
}

/**
 * @name Size
 * @brief Returns the number of elements in the List.
 */
template <typename T>
int List<T>::size() const {return theSize;}

/**
 * @name Empty
 * @brief Returns true if no element is in the list; otherwise, return false.
 */
template <typename T>
bool List<T>::empty() const {return size() == 0;}

/**
 * @name Clear
 * @brief Deletes all elements in the list.
 */
template <typename T>
void List<T>::clear()
{
	while(!empty())
		pop_front();
}

/**
 * @name Reverse
 * @brief Reverse the order of the elements in the list.
 */
template <typename T>
void List<T>::reverse()
{
	/** 
     * Inserts each element of the list at the end in reverse order. It then
     * uses the itr to delete the old Node.
     */
	for(auto itr = --end(); itr != --begin(); --itr)
	{
		insert(end(), *itr);
		erase(itr);
	}
}

/***************** front() and back() Definitions ****************************/

/**
 * @name Front
 * @brief Returns a reference to the first element in the list.
 */
template <typename T>
T& List<T>::front() {return *begin();}

/**
 * @name Constant Front
 */
template <typename T>
const T& List<T>::front() const {return *begin();}

/**
 * @name Back
 * @brief Returns a reference to the last element in the list. 
 */
template <typename T>
T& List<T>::back() {return *--end();}

/**
 * @name Constant Back
 */
template <typename T>
const T& List<T>::back() const {return *--end();}

/***************** Pop and Push Function Definitions *************************/
/**
 * @name (Copy) push_front 
 * @brief Insert the new object as the first element into the list.
 */
template <typename T>
void List<T>::push_front(const T & val) {insert(begin(), val);}

/**
 * @name (Move) push_front 
 */
template <typename T>
void List<T>::push_front(T && val) {insert(begin(), std::move(val));}

/**
 * @name (Copy) push_back 
 * @brief Inserts the new object as the last element into the list.
 */
template <typename T>
void List<T>::push_back(const T & val) {insert(end(), val);}

/**
 * @name (Move) push_back 
 */
template <typename T>
void List<T>::push_back(T && val) {insert(end(), std::move(val));}

/**
 * @name pop_front 
 * @brief Deletes the first element in the list.
 */
template <typename T>
void List<T>::pop_front() {erase(begin());}

/**
 * @name pop_back 
 * @brief Deletes the last element in the list.
 */
template <typename T>
void List<T>::pop_back() {erase(--end());}

/**
 * @name Remove
 * @brief Deletes all nodes with value equal to val from the list.
 */
template <typename T>
void List<T>::remove(const T &val)
{
	for(auto itr = begin(); itr != end(); ++itr)
	{
		if(val == *itr)
			erase(itr);
	}
}

/**
 * @name remove_if
 * @brief Deletes all nodes for which pred returns true.
 */
template<typename T>
template<typename PREDICATE>
void List<T>::remove_if(PREDICATE pred)
{
	for(auto itr = List<T>::begin(); itr != List<T>::end(); ++itr)
	{
		if(pred(*itr))
			List<T>::erase(itr);
	}
}

/**
 * @name Print
 * @brief Prints all elements in the list, using character ofc as the delimiter 
 *        between elements in the list.
 */
template <typename T>
void List<T>::print(std::ostream& os, char ofc) const
{
	for(auto itr = begin(); itr != end(); ++itr)
		os << *itr << ofc;

}

/************** Begin and End Function Definitions ***************************/

/**
 * @name Begin 
 * @brief Returns a iterator to the first element in the list.
 */
template <typename T>
typename List<T>::iterator List<T>::begin() {return iterator(head -> next);}

/**
 * @name (Constant) Begin
 */
template <typename T>
typename List<T>::const_iterator List<T>::begin() const 
{return const_iterator(head -> next);}

/**
 * @name End
 * @brief Returns a iterator to the end marker of the list (tail).
 */
template <typename T>
typename List<T>::iterator List<T>::end() {return iterator(tail);}

/**
 * @name (Constant) End 
 */
template <typename T>
typename List<T>::const_iterator List<T>::end() const
{return const_iterator(tail);}

/**
 * @name Insert
 * @brief Inserts a value val ahead of the node referred to by itr.
 */
template <typename T>
typename List<T>::iterator List<T>::insert(iterator itr, const T& val)
{
	Node *p = itr.current;
	++theSize;
	return(p -> prev = p -> prev -> next = new Node{val, p->prev, p});
}

/**
 * @name (Move) Insert 
 */
template <typename T>
typename List<T>::iterator List<T>::insert(iterator itr, T&& val)
{
	Node *p = itr.current;
	++theSize;
	return(p -> prev = p -> prev -> next = new Node{std::move(val), p->prev, p});
}

/**
 * @name Erase
 * @brief Deletes the node referred to by itr. The return value is an iterator 
 *        to the following node.
 */
template <typename T>
typename List<T>::iterator List<T>::erase(iterator itr)
{
	Node *p = itr.current;
	iterator retVal(p -> next);
	p -> prev -> next = p -> next;
	p -> next -> prev = p -> prev;
	delete p;
	--theSize;
	return retVal;
}

/**
 * @name Erase [start, end) 
 * @brief Deletes all the nodes between start and end (including start but not end), 
 *        that is, all elements in the range [start, end).
 */
template <typename T>
typename List<T>::iterator List<T>::erase(iterator start, iterator end)
{
	for(iterator itr = start; itr != end;)
		itr = erase(itr);
	
	return end;
}

/*****************************************************************************/
/************** Non-Class Function Definitions *******************************/
/*****************************************************************************/

/**
 * @name Operator==()
 * @brief Checks if two lists contain the same sequence of elements. Two lists 
 *        are equal if they have the same number of elements and the elements 
 *        at the corresponding position are equal.
 */
template <typename T>
bool operator==(const List<T> & lhs, const List<T> & rhs)
{
	/** Checks if the lists are equal lengths. */
	if(lhs.size() != rhs.size())
		return false;

	/** Used to move through the rhs list */
	auto itr2 = rhs.begin();
	for(auto itr = lhs.begin(); itr != lhs.end(); ++itr)
	{
		/** Checks if any element in the list is not equal. */
		if(*itr != *itr2)
			return false;

		++itr2;
	}
	return true;
}

/**
 * @name Operator!=()
 * @brief Opposite of operator==().
 */
template <typename T>
bool operator!=(const List<T> & lhs, const List<T> & rhs)
{
	if(lhs.size() != rhs.size())
		return true;

	auto itr2 = rhs.begin();
	for(auto itr = lhs.begin(); itr != lhs.end(); ++itr)
	{
		if(*itr != *itr2)
			return true;

		++itr2;
	}
	return false;
}

/**
 * @name Operator<<()
 * @brief Prints out all elements in list l by calling List<T>::print() function.
 */
template <typename T>
std::ostream & operator<<(std::ostream &os, const List<T> &l)
{
	l.print(os);
	return os;
}
