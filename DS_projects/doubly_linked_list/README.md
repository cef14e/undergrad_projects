Doubly Linked List
==================
This project focused on writing the definitions for the `list.h` class header
file. This is an implementation of a doubly linked list written in C++.

Compilation
-----------
To compile this program 'g++' was used. A `test_list.cpp` program is provided to
test the functionality of the list class. To compile the class with the `test_list.cpp`
program a makefile is provided. The makefile uses the following command:
```
g++ -Wall -std=c++11 test_list.cpp -o proj2.x
```

Usage
-----
In Linux the program can be executed using the fallowing command:
```
./proj2.x
```

Constant Iterator Functions
---------------------------
Below is a list of the function for the constant iterator class:

* **const_iterator()** : Default constructor, sets the current pointer to the nullptr
* **const_iterator(Node \*p)** : Constructor, sets the current pointer to the node passed in
* **operator\*()** : Returns a reference to the corresponding element in the list
* **operator++()** : Prefix increment operator, advance the pointer to the next node
* **operator--()** : Prefix decrement operator, move the pointer to the previous node
* **operator++(int)** : Postfix increment operator
* **operator--(int)** : Postfix decrement operator
* **operator==(const const_iterator &rhs) const** : Two iterators are equal if they 
                                                    refer to the same element
* **operator!=(const const_iterator &rhs) const** : Two iterators are not equal if 
                                                    they do not refer to the same element
* **retrieve() const** : Returns a reference to the corresponding element in the list

Iterator Functions
------------------
The iterator class is a child class of constant iterator.

* **iterator()** : Uses constant iterator's default constructor
* **iterator(Node \*p)** : Uses constant iterator's constructor to set the pointer.
* **operator\*() const**: Uses constant iterator's operator* function.
* **operator\*()** : Returns a reference to the corresponding element in the list
* **operator++()** : Prefix increment operator, advance the pointer to the next node
* **operator--()** : Prefix decrement operator, move the pointer to the previous node
* **operator++(int)** : Postfix increment operator
* **operator--(int)** : Postfix decrement operator

List Functions
--------------
Below is a list of functions the list class uses:

* **init()** : Initialize the member variables of list
* **List()** : Default constructor, calls the 'init' function
* **List(int num, const T& val)** : Constructs a list
    * **num** : Number of element to construct the list with
    * **val** : The value to set every element of the list to.

* **~List()** : Destructor
* **List(const List &rhs)** : Copy constructor
    * **rhs** : The list to be copied

* **List(List && rhs)** : Move constructor
* **List(const_iterator start, const_iterator end)** : (Parameter) Copy constructor, 
             Construct List with elements from another list [star,end) iterators
    * **start** : The starting iterator of the list to be copied
    * **end** : The end of the list to be copied

* **List(std::initializer_list<T> iList)**: (Initializer List) constructor, Construct 
                                            list using a initializer list
* **operator=(const List<T> &rhs)** : Copy assignment operator
* **operator=(List &&rhs)** : Move Assignment
* **operator=(std::initializer_list<T> iList)** : Initializer list assignment operator
* **size()** : Returns the number of elements in the list
* **empty()** : True if no element is in the list, false otherwise
* **clear()** : Deletes all elements in the list
* **reverse()** : Reverse the order of the elements in the list
* **front()** : Returns a reference to the first element in the list
* **front() const** : Constant front function
* **back()** : Returns a reference to the last element in the list
* **back() const** : Constant back function
* **push_front(const T & val)** : Inserts val at the front of the list
* **push_front(T && val)** : Move push_front function
* **push_back(const T & val)** : Insets val at the end of the list
* **push_back(T && val)** : Move push_back function
* **pop_front()** : Deletes the first element in the list
* **pop_back()** : Deletes the last element in the list
* **remove(const T &val)** : Deletes all nodes with value equal to val from the list
* **template<typename PREDICATE> remove_if(PREDICATE pred)** : Deletes all nodes for which pred
                                                               returns true
* **print(std::ostream& os, char ofc) const** : Prints all elements in the list
    * **ofc** : Used as a delimiter between characters in the list

* **begin()** : Returns a iterator to the first element in the list
* **begin() const** : Constant begin function
* **end()** : Returns a iterator to the end marker of the list (tail)
* **end() const** : Constant end function
* **insert(iterator itr, const T& val)** : Inserts a value into the list
    * **itr** : The iterator where 'val' will be inserted
    * **val** : The value to insert at 'itr'

* **insert(iterator itr, T&& val)** : Move insert function
* **erase(iterator itr)** : Deletes the node of the list referred to by 'itr', returns
                            an iterator to the following node
* **erase(iterator start, iterator end)** : Deletes each node in between start and end,
                                            including start but not end


Non-Class Functions
-------------------
Below is a list of the non-class functions:

* **operator==(const List<T> & lhs, const List<T> & rhs)** : Checks if two lists
                                           contain the same sequence of elements
    * Two lists are equal if they have the same number of elements and the elements
      at the corresponding position are equal

* **operator!=(const List<T> & lhs, const List<T> & rhs)** : The opposite of 'operator=='
* **& operator<<(std::ostream &os, const List<T> &l)** : Prints out all elements in 
                                        list l by calling List<T>::print() function
