#include "stack.h"

/**
 * This file contains the definition for the class stack.h
 *
 * Camill Folsom
 * June 30, 2017
 */

/**
 * @name (Default) Constructor
 */
template <typename T, class Container>
Stack<T, Container>::Stack() : c{} {};

/**
 * @name Destructor
 */
template <typename T, class Container>
Stack<T, Container>::~Stack()
{clear();}

/**
 * @name Copy Constructor
 */
template <typename T, class Container>
Stack<T, Container>::Stack(const Stack<T, Container>& rhs) : c{rhs.c} {};

/**
 * @name Move Constructor
 */
template <typename T, class Container>
Stack<T, Container>::Stack(Stack<T, Container> && rhs) : c{std::move(rhs.c)} {};

/**
 * @name Copy Assignment Operator
 */
template <typename T, class Container>
Stack<T, Container>& Stack<T, Container>::operator=(const Stack<T, Container>& rhs) 
{
	c = rhs.c;
	return *this;
}

/**
 * @name Move Assignment Operator
 */
template <typename T, class Container>
Stack<T, Container>& Stack<T, Container>::operator=(Stack<T, Container>&& rhs)
{
	c = std::move(rhs.c);
	return *this;
}

/**
 * @name Empty
 * @brief Returns true if the stack is empty.
 */
template <typename T, class Container>
bool Stack<T, Container>::empty() const {return c.empty();}

/**
 * @name Clear
 * @brief Removes everything from the stack.
 */
template <typename T, class Container>
void Stack<T, Container>::clear() {c.clear();}

/**
 * @name (Copy) Push
 * @brief Push an element onto the top of the stack. This stack is using
 * 		  the back of the container as the top of the stack
 */
template <typename T, class Container>
void Stack<T, Container>::push(const T& x) {c.push_back(x);}

/**
 * @name (Move) Push
 * @brief Move version of the push function defined above.
 */
template <typename T, class Container>
void Stack<T, Container>::push(T&& x) {c.push_back(std::move(x));}

/**
 * @name Pop
 * @brief Deletes the top of the stack for this stack that is the back of the
 * 		  container.
 */
template <typename T, class Container>
void Stack<T, Container>::pop() {c.pop_back();}

/**
 * @name (Copy) Top
 * @brief Returns a reference to the top of the stack which is the back of the
 * 		  container.
 */
template <typename T, class Container>
T& Stack<T, Container>::top() { return c.back();}

/**
 * @name (Move) Top
 * @brief Move version of top defined above.
 */
template <typename T, class Container>
const T& Stack<T, Container>::top() const {return top();}

/**
 * @name Size
 * @brief Returns the size of the stack.
 */
template <typename T, class Container>
int Stack<T, Container>::size() const {return c.size();}

/**
 * @name Print
 * @brief Prints the stack in reverse order i.e. the first element inserted
 * 		  into the stack is the first element printed.
 */
template <typename T, class Container>
void Stack<T, Container>::print(std::ostream& os, char ofc) const
{
	for(auto it = c.begin(); it != c.end(); ++it)
		os << *it << ofc;
}

/**
 * @name Operator<<
 * @brief Calls the stacks print function to print the stack.
 */
template <typename T, class Container>
std::ostream& operator<<(std::ostream& os, const Stack<T, Container>& a)
{
	a.print(os);
	return os;
}

/**
 * @name Operator==
 * @brief Return true if two stacks contain the same elements in the same order.
 */
template <typename T, class Container>
bool operator==(const Stack<T, Container>& rhs, const Stack<T, Container>& lhs)
{
	if(rhs.size() != lhs.size())
		return false;

	auto it2 = lhs.c.begin();
	for(auto it = rhs.c.begin(); it != rhs.c.end(); ++it)
	{
		if(*it != *it2)
			return false;

		++it2;
	}
	return true;	
}

/**
 * @name Operator!=
 * @brief Return true if two stacks do not contain all the same elements.
 */
template <typename T, class Container>
bool operator!=(const Stack<T, Container>& rhs, const Stack<T, Container>& lhs)
{return !operator==(rhs, lhs);}

/**
 * @name Operator<=
 * @brief Return true if everything in stack a is smaller than or equal to
 * 		  stack b.
 */
template<typename T, class Container>
bool operator<=(const Stack<T, Container>& a, const Stack<T, Container>& b)
{
	auto it2 = b.c.begin();
	for(auto it = a.c.begin(); it != a.c.end(); ++it)
	{
		if(it2 == b.c.end())
			break;

		if(*it > *it2)
			return false;

		++it2;
	}
	return true;
}
