#include <iostream>
#include <string>
#include <queue>
#include "stack.h"

/**
 * This program uses a stack to convert infix notation to postfix notation.
 * it then evaluates the postfix expression.
 *
 * Camill Folsom
 * June 30, 2017
 */

/**
 * @name Higher Priority
 * @brief Checks if the top of the stack has higher priority than the input 
 *        character
 * @param top is a reference to the top of the stack, val is the current value
 * 		  being evaluated.
 */
bool higher_priority(const std::string & top, const std::string & val)
{
	if(top == "+" || top == "-")
	{
		if(val == "*" || val == "/")
			return false;
	}

	if(top == "(" || val.empty())
		return false;

	return true;
}

/**
 * @name Parse Line
 * @brief Goes through a line of input character by character performing
 * 		  infix to postfix conversion. This function returns true if a error
 * 		  occurs and false otherwise.
 * @param line - is the line currently being read in from input.
 * 		  str_stack - is a stack this is used to hold the operators.
 * 		  copy - is used to make a copy of the output so it can be used to
 * 		  		 evaluate the expression after Parse Line has finished.
 * 		  is_num - is used to determine if an input line contains anything
 * 		  		   that is not operators or digits
 */
bool parse_line(std::string & line, cop4530::Stack<std::string> & str_stack, 
				std::queue<std::string> & copy, bool & is_num)
{
	/** Used to make the value string, and used to hold the previous value */
	std::string val, prev_val;
	/** Used to make sure the last string is read */
	bool check = true;
	/** A iterator to the last value in the line */
	auto last = *--line.end();

	/** Make sure the end of line is not ( or an operand. */
	if(last == '(' || last == '+' || last == '-' || last == '/' || last == '*')
	{
		std::cout << "\nError the last input was '(' or a operand." << std::endl;
		return true;
	}

	for(auto it = line.begin(); it != line.end(); ++it)
	{

		/** 
 		 * Checks each character in the line to see if there are any values
 		 * that are not numerical, so we can see if we can evaluate the expression.
 		 */
		if(!isdigit(*it) && *it != '+' && *it != '-' && *it != '/'
		   && *it != '*' && *it != '(' && *it != ')' && *it != '.' && *it != ' ')
		{
				is_num = false;	
		}

		/** Makes sure the last value gets parsed */
		if(it == --line.end())
		{			
			if(*--line.end() != ' ')
			{
				val += *it;
				/** Makes the next if statement false so the value is parsed. */
				check = false;
			}
		}

		/** Makes the string, then parses the string when a space is hit. */
		if(*it != ' ' && check)
		{
			val += *it;
		}

		/** Checks that if val needs to be output. */
		else if(val != "+" && val != "-" && val != "*" && val != "/" 
		        && val != "(" && val != ")" && !val.empty())
		{
			std::cout << val << " ";
			
			/** 
 			 * If both the previous value and this value are not operators or (
 			 * then we have an error becuase we need anoth operator i.e. 1 2 + 3.
 			 */ 
			if(prev_val != "+" && prev_val != "-" && prev_val != "*" && 
     		   prev_val != "/" && prev_val != "(" && prev_val != ")" && !prev_val.empty())
			{
				std::cout << "\nError an operand is missing a operator.\n";
				return true;
			}

			copy.push(val);
		}

		/** Pushes operators to the stack if it is empty */
		else if(str_stack.size() == 0 && !val.empty() && val != ")")
			str_stack.push(val);

		else if(val == "(")
			str_stack.push(val);

		else if(val == ")")
		{
			/** Makes sure ) is not the only thing in the stack. */
			if(str_stack.empty())
			{
				std::cout << "\nError no '(' found for a ')'." << std::endl;
				return true;
			}

			/** Checks that no operand proceeds a ). */
			if(prev_val == "+" || prev_val == "-" || prev_val == "/" || 
			   prev_val == "*")
			{
				std::cout << "\nError an operand proceeds ')'" << std::endl;
				return true;
			}

			/** Print and pop the stack until we hit a ( */
			while(str_stack.top() != "(")
			{
				/** Checks to make sure the ) has and ( */
				if(str_stack.empty())
				{
					std::cout << "\nError no '(' found for a ')'." << std::endl;
					return true;
				}			

				std::cout << str_stack.top() << " ";
				copy.push(str_stack.top());	
				str_stack.pop();
			}
			/** Takes care of the ( */
			str_stack.pop();
		}

		/**
 		 * Calls higher priority to determine if the top of the stack has
 		 * higher priority than val if it does we pop the stack till empty
 		 * and then push val onto the stack. The loop exits if we find a (
 		 */
		else if(higher_priority(str_stack.top(), val))
		{
			while(!str_stack.empty())
			{
				/** Exits the loop if we hit a ( */
				if(str_stack.top() == "(")
					break;

				std::cout << str_stack.top() << " ";
				copy.push(str_stack.top());	
				str_stack.pop();
			}
			str_stack.push(val);
		}
		
		/** 
 	 	 * If val has a higher priority than the top of the stack then it is
 	 	 * pushed onto the stack.
  		 */
		else if(!val.empty())
			str_stack.push(val);			

		/**
 		 * Whenever a space is encounted we set prev val and clear val
 		 * to ensure that val is each string separated by spaces.
 		 */
		if(*it == ' ')
		{
			prev_val = val;
			val.clear();
		}
	}
	/** Returns false to indicated no error. */
	return false;
}

int main ()
{
	/** Used to read input */
	std::string line;
	/** Used to determine if we can evaluated the expression */
	bool is_num = true;
	/** Used to determine if a error occured */
	bool error = false;
	/** Used to store the values to be evaluated */
	cop4530::Stack<float> num_stack;
	/** Used to convert from infix to postfix */
	cop4530::Stack<std::string> str_stack;
	/** Used to copy the strig stack so we can evaluate it */
	std::queue<std::string> copy, copy2;	
	float result;

	std::cout << "Enter infix expression (\"exit\" to quit):\n";	

	while(std::getline(std::cin, line))
	{
		if(line == "exit")
			break;

		std::cout << "Postfix expression: ";

		error = parse_line(line, str_stack, copy, is_num);

		if(error)
			break;

		/** Makes sure if any operators are still in the stack they get printed */		
		while(!str_stack.empty())
		{
			std::cout << str_stack.top() << " ";
			copy.push(str_stack.top());
			str_stack.pop();
		}		

		std::cout << "\nPostfix evaluation: ";

		/** Makes sure we can evaluated the expression */
		if(is_num)
		{
			/** Parses the copy of the output so the expression can be evaluated */
			while(!copy.empty())
			{
				/** Pushed any numbers to the num_stack as a float */
				if(copy.front() != "+" && copy.front() != "-" && copy.front() != "/"
				   && copy.front() != "*")
					num_stack.push(stof(copy.front()));

				/**
 				 * The next 4 else if statements deal with hitting a operator
 				 * in the postfix expression. All 4 statements do the desired
 				 * operations by getting the first two number and performing
 				 * the operation on them.  
 				 */ 
				else if(copy.front() == "+" && num_stack.size() > 1)
				{
					/** Sets result equal to the top of the stack */
					result = num_stack.top();
					num_stack.pop();
					/** Adds the result to the next top of the stack */
					result += num_stack.top();
					num_stack.pop();
					/** Puts the results of the operation back in num_stack */
					num_stack.push(result);
					result = 0.0;
				}
	
				else if(copy.front() == "-" && num_stack.size() > 1)
				{
					result = num_stack.top();
					num_stack.pop();
					result = num_stack.top() - result;
					num_stack.pop();
					num_stack.push(result);
					result = 0.0;
				}

				else if(copy.front() == "/" && num_stack.size() > 1)
				{
					result = num_stack.top();
					num_stack.pop();
					result = num_stack.top() / result;
					num_stack.pop();
					num_stack.push(result);
					result = 0.0;
				}
				else if(copy.front() == "*" && num_stack.size() > 1)
				{
					result = num_stack.top();
					num_stack.pop();
					result *= num_stack.top();
					num_stack.pop();
					num_stack.push(result);
					result = 0.0;
				}
			
				std::cout << copy.front() << " ";
				copy.pop();
			}
			std::cout << "= " << num_stack.top();
		}

		/** Take care of the case where we cannot evaluate the expression */
		else
		{
			/** Makes a copy so we can print the contents of the queue twice */
			copy2 = copy;
			while(!copy.empty())
			{
				std::cout << copy.front() << " ";
				copy.pop();
			}
			std::cout << "= ";
			while(!copy2.empty())
			{
				std::cout << copy2.front() << " ";
				copy2.pop();
			}
		}
			

		std::cout << "\nEnter infix expression (\"exit\" to quit):\n";
	}

	return 0;
}
