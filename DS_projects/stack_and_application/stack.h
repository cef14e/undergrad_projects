#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <deque>

/**
 * This is a stack class that creates a stack from a Container.
 * If no Container is specified then deque is used by default.
 * This class can by used with any STL Container that contains
 * the functions:
 * push_back
 * pop_back
 * back
 * empty
 * clear
 * size
 * begin
 * end
 *
 * Camill Folsom
 * June 30, 2017
 */

namespace cop4530
{
	template <typename T, class Container = std::deque<T>>
	class Stack
	{
		public:
			Stack();								/* Zero parameter constructor */
			~Stack();								/* Destructor */
			Stack(const Stack<T, Container>&);		/* Copy Constructor */
			Stack(Stack<T, Container> &&);			/* Move Constructor */
			Stack<T, Container>& operator=(const Stack<T, Container>&);	 /* Copy Assignment Operator */
			Stack<T, Container>& operator=(Stack<T, Container>&&);		 /* Move Assignment Operator */
			bool empty() const;						/* True if stack contains nothing */
			void clear();							/* Deletes everything in stack */
			void push(const T& x);					/* Add x to stack. Copy Version */
			void push(T&& x);						/* Add x to stack. Move Version */
			void pop();								/* Removes last added element */
			T& top();								/* Last added element to stack */
			const T& top() const;					/* Accessor for top() */
			int size() const;						/* Number of things in stack */
			void print(std::ostream& os, char ofc = ' ') const;		/* Prints stacks separated by ofc */

		private:
			Container c;
	
		template <typename W, class Con>
		friend bool operator==(const Stack<W, Con>&, const Stack<W, Con>&);

		template <typename W, class Con>
		friend bool operator<=(const Stack<W, Con>& a, const Stack<W, Con>& b);
	};

	/* Calls print to print stack */
	template <typename T, class Container>
	std::ostream& operator<<(std::ostream& os, const Stack<T, Container>& a);

	/* True if two stacks contian the same elements */
	template <typename T, class Container>
	bool operator==(const Stack<T, Container>&, const Stack<T, Container>&);

	template <typename T, class Container>
	bool operator!=(const Stack<T, Container>&, const Stack<T, Container>&);

	/* True if everything in a is smaller or equal to b */
	template <typename T, class Container>
	bool operator<=(const Stack<T, Container>& a, const Stack<T, Container>& b);

	#include "stack.hpp"
}	//end of namespace 4530

#endif
