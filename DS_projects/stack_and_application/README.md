Stack and Its Application
=========================
This project involved the creation of a generic stack container as an adapter
class template. The stack was then used to create a program that reads in a infix 
expression evaluates it and converts the expression to a postfix expression, 
`in2post.cpp`.

Compilation
-----------
To compile a makefile is provided. The makefile compiles the `in2post.cpp` program
using the following command:
```
g++ -Wall -std=c++11 in2post.cpp -o in2post.x
```

This requires the files `stack.h` and `stack.cpp` to be in the same directory
as the `in2post.cpp` file. This is because the `in2post.cpp` program uses the
custom made stack class.

Usage
-----
To execute this program in Linux use the following command:
```
./in2post.x
```

in2post.cpp
-----------
This program takes user entered infix expressions evaluates them and converts
them into postfix expressions. The infix expression will only be evaluated if
all the operands are numeric values. Otherwise, if the resulting postfix expression 
contains characters, the evaluation will be the postfix expression.

Example
-------
The following infix expressions are used in the example:

* ( 5 + 3 ) * 12  - 7 = 89
* 5 + 3 * 12 - 7 = 34
* a + b1 * c + ( dd * e + f ) * G
* ( 3 * 5 - c ) / 10

Below is an example run of the program using the above infix expressions:
```text
Enter infix expression ("exit" to quit):
( 5 + 3 ) * 12  - 7
Postfix expression: 5 3 + 12 * 7 -
Postfix evaluation: 5 3 + 12 * 7 - = 89
Enter infix expression ("exit" to quit):
5 + 3 * 12 - 7
Postfix expression: 5 3 12 * + 7 -
Postfix evaluation: 5 3 12 * + 7 - = 34
Enter infix expression ("exit" to quit):
a + b1 * c + ( dd * e + f ) * G
Postfix expression: a b1 c * + dd e * f + G * +
Postfix evaluation: a b1 c * + dd e * f + G * + = a b1 c * + dd e * f + G * +
Enter infix expression ("exit" to quit):
( 3 * 5 - c ) / 10
Postfix expression: 3 5 * c - 10 /
Postfix evaluation: 3 5 * c - 10 / = 3 5 * c - 10 /
Enter infix expression ("exit" to quit):
exit
```

Interface
---------
This section list the functions inside the stack class. The class has the template
below:
```
template <typename T, class Container>
```

Here 'T' is a variable type and 'Container' is the underlying container the stack
uses, i.e. the classes private variable it uses as a container.
Below is an example of creating two stacks:
```
cop4530::Stack<int> stack;
cop4530::Stack<float, std::list<float>> stack;
```

The first line creates a stack of type int using a deque as its underlying container.
The second line creates a stack of type float using a list as the underlying container.
For this class the back of the container is consider the top of the stack.

Below is a list of each function used by the `Stack.h` class.

* **Stack():** zero-argument constructor
* **~Stack():** destructor  
* **Stack(const Stack<T, Container>& rhs):** copy constructor
* **Stack(Stack<T, Container> && rhs):** move constructor  
* **Stack<T, Container>& operator=(const Stack<T, Container>& rhs):** copy assignment operator=
* **Stack<T, Container>& operator=(Stack<T, Container>&& rhs):** move assignment operator=
* **bool empty() const:** returns true if the Stack contains no elements, and false otherwise.   
* **void clear():** delete all elements from the stack
* **void push(const T& x):** adds x to the Stack, copy version
* **void push(T&& x):** adds x to the Stack, move version
* **void pop():** removes and discards the most recently added element of the Stack
* **T& top():** returns a reference to the most recently added element of the Stack 
* **const T& top() const:** accessor that returns the most recently added element 
                            of the Stack (as a const reference) 
* **int size() const:** returns the number of elements stored in the Stack
* **void print(std::ostream& os, char ofc) const:** print elements of Stack 
                            to ostream os. ofc is the separator between elements 
                            in the stack when they are printed out. 
    * **Note:** print() prints elements in the opposite order of the Stack (that 
                is, the oldest element is printed first).

* **std::ostream& operator<<(std::ostream& os, const Stack<T, Container>& a):** 
    invokes the print() method to print the Stack<T, Container> a in the specified ostream
* **bool operator==(const Stack<T, Container>& rhs, const Stack<T, Container>& lhs):**
    returns true if the two compared Stacks have the same elements, in the same 
    order, and false otherwise 
* **bool operator!=(const Stack<T, Container>& rhs, const Stack<T, Container>& lhs):** opposite of operator==()
* **bool operator<=(const Stack<T, Container>& a, const Stack<T, Container>& b):** 
    returns true if every element in Stack a is smaller than or equal to the corresponding 
    element of Stack b
