#include "passserver.h"
#include <string>
#include <crypt.h>
#include <utility>
#include <unistd.h>
#include <cstring>

/**
 * Definitions for the PassServer class.
 *
 * Camill Folsom
 * July 18, 2018
 */


/**
 * @name (Default) Constructor
 * @brief Creates a table of size. Since in HashTable prime_below determines
 * 		  the size so the size of the table could be different than the parameter.
 */
PassServer::PassServer(size_t size) : table{size} {}

/**
 * @name Destructor
 */
PassServer::~PassServer()
{ table.clear(); }

/**
 * @name Load
 * @brief Loads a file where each line contains a single username-encrypted password
 * 		  separated by a space.
 */
bool PassServer::load(const char *filename)
{ 
	if(table.load(filename))
		return true;

	return false; 
}

/**
 * @name Add User
 * @brief Adds a username and encrypted password to the table. 
 */
bool PassServer::addUser(const std::pair<std::string, std::string> & kv)
{
	std::string password = encrypt(kv.second);
	std::pair<std::string, std::string> p1(kv.first, password);

	if(table.insert(p1))
		return true;

	return false;
}

/**
 * @name (Move) Add User
 * @brief Move Version of Add User.
 */
bool PassServer::addUser(std::pair<std::string, std::string> && kv)
{
	std::string password = encrypt(std::move(kv.second));
	std::pair<std::string, std::string> p1(std::move(kv.first), password);

	if(table.insert(p1))
		return true;

	return false;
}

/**
 * @name Remove
 * @brief Removes the user k
 */
bool PassServer::removeUser(const std::string & k)
{
	if(table.remove(k))
		return true;

	return false;
}

/**
 * @name ChangePassword
 * @brief 
 */
bool PassServer::changePassword(const std::pair<std::string, std::string> & p, 
								const std::string & newpassword)
{	
	if(!table.contains(p.first))
		return false;

	std::string pass = encrypt(p.second);
	std::pair<std::string, std::string> p2(p.first, pass);

	if(!table.match(p2))
		return false;

	pass = encrypt(newpassword);
	p2 = std::make_pair(p.first, pass);

	/** Updates the table with the new password */
	table.insert(p2);

	return true;
}

/**
 * @name Find
 * @brief Checks to see if user is in the table
 */
bool PassServer::find(const std::string & user)
{
	if(table.contains(user))
		return true;
	
	return false;
}

/**
 * @name Dump
 * @brief Displays the table. It uses the same format as HashTable's dump.
 */
void PassServer::dump()
{ table.dump(); }

/**
 * @name Size
 * @brief Returns the size of the table (the number of pairs in the table).
 */
size_t PassServer::size()
{ return table.size(); }

/**
 * @name Table Size
 * @brief Returns the size of the table.
 */
size_t PassServer::tableSize()
{ return table.tableSize(); }

/**
 * @name Write To File
 * @brief Writes the username - encrypted password pair to a file. Each line
 * 		  of the file contains one pair and the pair is separated by whitespace.
 */
bool PassServer::write_to_file(const char *filename)
{
	if(table.write_to_file(filename))
		return true;

	return false;
}

/**
 * @name Encrypt
 * @brief Encrypt the str parameter and return an encrypted string.
 */
std::string PassServer::encrypt(const std::string & str)
{
	char salt[] = "$1$########";
	std::string password;	

	char * pass = new char[100];
	strcpy(pass, crypt(str.c_str(), salt));

	password = pass;

	/** Gets rid of salt from password string before adding to table */
	password = password.substr(12, password.size());

	delete [] pass;	

	return password;
}
