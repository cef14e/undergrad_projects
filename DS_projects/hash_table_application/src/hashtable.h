#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <list>

/**
 * Declaration for the HashTable class.
 *
 * Camill Folsom
 * July 18, 2018
 */

// max_prime is used by the helpful functions provided
// to you.
static const unsigned int max_prime = 1301081;

// the default_capacity is used if the initial capacity 
// of the underlying vector of the hash table is zero. 
static const unsigned int default_capacity = 11;
 

namespace cop4530
{
	template <typename K, typename V>
	class HashTable
	{
		public:
			HashTable(size_t size = 101);			/** Parameter Constructor */
			~HashTable();							/** Destructor */
			bool contains(const K & k);				/** Checks if k is in hash table */
			bool match(const std::pair<K, V> & kv);	/** Checks if key-value pair is in the hash table */
			bool insert(const std::pair<K, V> & kv);	/** Adds key-value pair to the table */
			bool insert(std::pair<K, V> && kv);		/** Move version of insert */
			bool remove(const K & k);				/** Deletes the key k and its value */
			void clear();							/** Deletes all elements in table */
			bool load(const char *filename);		/** Loads contents of file into table */
			void dump();							/** Display all table entries */
			bool write_to_file(const char *filename); /** Write table to file */
			size_t size();							/** Give number of elements in table */
			size_t tableSize();						/** Returns the size of the vector */		

			class CompareKey
			{
				public:
					CompareKey(const K);
					bool operator()(const std::pair<K, V> & kv); /** Compare key to key value in pair */

				private:
					const K key;					/** The key that is compared to the pairs key */
			};

			class ComparePair
			{
				public:
					ComparePair(const std::pair<K, V> kv);
					bool operator()(const std::pair<K, V> & kv); /** Compares two pairs */

				private:
					const K key;					/** The key that is compared to the pairs key */
					const V val;					/** The value that is compared to the pairs value */
			};


		private:
			std::vector<std::list<std::pair<K, V>>> theLists; /** The vector of lists */
			size_t currentSize;						/** Current number of elements in the table */

			void makeEmpty();						/** Makes table empty */
			void rehash();							/** Used when more elements than vector size */
			size_t myhash(const K & k);				/** Index of vector entry where k is stored */
			unsigned long prime_below(unsigned long);
			void setPrimes(std::vector<unsigned long>&);
	};

	#include "hashtable.hpp"
} // end of cop4530 namespace

#endif
