#include "hashtable.h"

/**
 * Definitions for the HashTable class. Function  prime_below and setPrimes 
 * written by Bob Myers
 *
 * Camill Folsom
 * July 18, 2018
 */

/******************************************************************************/
/********************* CompareKey Class Definitions ***************************/
/******************************************************************************/

/**
 * @name (Default) Constructor - CompareKey Class
 * @brief Sets the key value.
 */
template <typename K, typename V>
HashTable<K, V>::CompareKey::CompareKey(const K k) : key(k) {};

/**
 * @name Operater() - CompareKey Class
 * @brief Compares the key value to the pairs key value.
 */
template <typename K, typename V>
bool HashTable<K, V>::CompareKey::operator()(const std::pair<K, V> & kv)
{ return kv.first == key; }

/******************************************************************************/
/******************** ComparePair Class Definitions ***************************/
/******************************************************************************/

/**
 * @name (Default) Cosntructor - ComparePair Class
 * @brief Sets the key and val values.
 */
template <typename K, typename V>
HashTable<K, V>::ComparePair::ComparePair(const std::pair<K, V> kv) 
									    : key(kv.first), val(kv.second) {};

/**
 * @name Operator() - ComparePair Class
 * @brief Compares two pairs.
 */
template <typename K, typename V>
bool HashTable<K, V>::ComparePair::operator()(const std::pair<K, V> & kv)
{
	if(kv.first != key)
		return false;

	return kv.second == val;
}

/******************************************************************************/
/********************* HashTable Class Definitions ****************************/
/******************************************************************************/

/**
 * @name (Paramater) Constructor
 * @brief Makes the vector the size given by the prime_below function. 
 */
template <typename K, typename V>
HashTable<K, V>::HashTable(size_t size)
{
	theLists.resize(prime_below(size));
	currentSize = 0;
}

/**
 * @name Destructor
 * @brief Calls makeEmpty to clear the table.
 */
template <typename K, typename V>
HashTable<K, V>::~HashTable() { makeEmpty(); }

// returns largest prime number <= n or zero if input is too large
// This is likely to be more efficient than prime_above(), because
// it only needs a vector of size n
template <typename K, typename V>
unsigned long HashTable<K, V>::prime_below (unsigned long n)
{
  if (n > max_prime)
    {
      std::cerr << "** input too large for prime_below()\n";
      return 0;
    }
  if (n == max_prime)
    {
      return max_prime;
    }
  if (n <= 1)
    {
		std::cerr << "** input too small \n";
      return 0;
    }

  // now: 2 <= n < max_prime
  std::vector <unsigned long> v (n+1);
  setPrimes(v);
  while (n > 2)
    {
      if (v[n] == 1)
	return n;
      --n;
    }

  return 2;
}

//Sets all prime number indexes to 1. Called by method prime_below(n) 
template <typename K, typename V>
void HashTable<K, V>::setPrimes(std::vector<unsigned long>& vprimes)
{
  int i = 0;
  int j = 0;

  vprimes[0] = 0;
  vprimes[1] = 0;
  int n = vprimes.capacity();

  for (i = 2; i < n; ++i)
    vprimes[i] = 1;

  for( i = 2; i*i < n; ++i)
    {
      if (vprimes[i] == 1)
        for(j = i + i ; j < n; j += i)
          vprimes[j] = 0;
    }
}

/**
 * @name makeEmpty
 * @brief Deletes all the elements in the table.
 */
template <typename K, typename V>
void HashTable<K, V>::makeEmpty()
{
	for(auto & aList : theLists)
		aList.clear();

	currentSize = 0;
}

/**
 * @name Rehash
 * @brief Called when the number of elements in the table are greater than the vectors size.
 */
template <typename K, typename V>
void HashTable<K, V>::rehash()
{
	std::vector<std::list<std::pair<K, V>>> oldLists = theLists;

	/** Makes the size of the table the first prime that is twice as big as the old */
	theLists.resize(prime_below(2 * theLists.size()));
	makeEmpty();

	/** Copy the old lists to the new vector */
	for(auto & aList: oldLists)
		for(auto & x : aList)
			insert(std::move(x));
}

/**
 * @name myhash
 * @brief Returns the index to the vector entry where k should be stored.
 */
template <typename K, typename V>
size_t HashTable<K, V>::myhash(const K  & k)
{
	static std::hash<K> hf;
	return hf(k) % theLists.size();
}

/**
 * @name Contains
 * @brief Checks if key k is in the table.
 */
template <typename K, typename V>
bool HashTable<K, V>::contains(const K & k)
{
	auto & aList = theLists[myhash(k)];
	return std::find_if(aList.begin(), aList.end(), CompareKey(k)) != aList.end();
}

/**
 * @name Match
 * @brief Checks if the key-value pair is in the table
 */
template <typename K, typename V>
bool HashTable<K, V>::match(const std::pair<K, V> & kv)
{
	auto & aList = theLists[myhash(kv.first)];
	return std::find_if(aList.begin(), aList.end(), ComparePair(kv)) != aList.end();
}

/**
 * @name Insert
 * @brief Adds the key-value pair to the table. If kv is already in the table
 * 		  do not add it. If the key is in the table but the value is different
 * 		  then it updates the value with the new one. Returns true if pair
 * 		  is added or value is updated otherwise it returns false.
 */
template <typename K, typename V>
bool HashTable<K, V>::insert(const std::pair<K, V> & kv)
{
	/** Checks if kv is in the table */
	if(match(kv))
		return false;

	/** Checks to see if the value needs to be updates */
	if(contains(kv.first))
	{
		remove(kv.first); 
		/** Recursively call insert to update the key-value pair */
		insert(kv);
		return true;
	}

	auto & aList = theLists[myhash(kv.first)];

	aList.push_back(kv);

	/** Rehashes if needed */
	if(++currentSize > theLists.size())
		rehash();

	return true;
}

/**
 * @name (Move) Insert
 * @brief Move Version of Insert
 */
template <typename K, typename V>
bool HashTable<K, V>::insert(std::pair<K, V> && kv)
{
	/** Checks if kv is in table */
	if(match(kv))
		return false;

	/** Checks to see if the value needs to be updates */
	if(contains(kv.first))
	{
		remove(kv.first); 
		/** Recursively call insert to update the key-value pair */
		insert(kv);
		return true;
	}
	auto & aList = theLists[myhash(kv.first)];

	aList.push_back(std::move(kv));

	/** Rehash if needed */
	if(++currentSize > theLists.size())
		rehash();

	return true;
}

/**
 * @name Remove
 * @brief Deletes the key k and its value from the table. Returns true only
 * 		  if the pair is deleted.
 */
template <typename K, typename V>
bool HashTable<K, V>::remove(const K & k)
{
	auto & aList = theLists[myhash(k)];
	auto it = std::find_if(aList.begin(), aList.end(), CompareKey(k));

	if(it == aList.end())
		return false;

	aList.erase(it);
	--currentSize;
	return true;
}

/**
 * @name Clear
 * @brief Deletes all elements in table by calling makeEmpty.
 */
template <typename K, typename V>
void HashTable<K, V>::clear() { makeEmpty(); }

/**
 * @name Load
 * @brief Loads the contents of a file into the table. In the file, each line
 * 		  contains a single key-value pair, separated by whitespace.
 */

template <typename K, typename V>
bool HashTable<K, V>::load(const char *filename)
{
	std::ifstream file(filename);
	std::string line;
	K key;
	V val;

	if(file.is_open())
	{
		while(getline(file, line))
		{
			std::stringstream line_copy(line);
			line_copy >> key;
			line_copy >> val;
			insert(std::make_pair(key, val));
		}
		file.close();
		return true;
	}

	return false;
}

/**
 * @name Write To File
 * @brief Writes the table to a file with the format used for load.
 */
template <typename K, typename V>
bool HashTable<K, V>::write_to_file(const char *filename)
{
	std::ofstream file(filename);

	if(file.is_open())
	{
		for(size_t i = 0; i < theLists.size(); ++i)
		{
			auto & aList = theLists[i];

			for(auto x : aList)
				file << x.first << " " << x.second << std::endl;
		}
		file.close();
		return true;
	}

	return false;
}

/**
 * @name Dump
 * @brief Displays the contents of the table. If a entry contains multiple
 * 		  key-value pairs it displays them separated by a colon (:)
 */
template <typename K, typename V>
void HashTable<K, V>::dump()
{
	int count = 0;

	for(size_t i = 0; i < theLists.size(); ++i)
	{
		auto & aList = theLists[i];
		std::cout << "v[" << i << "]: ";

		for(auto x : aList)
		{
			if(count > 0)
				std::cout << ":";

			std::cout << x.first << " " << x.second;

			++count;
		}
		std::cout << std::endl;
		count = 0;
	}
}

/**
 * @name Size
 * @brief Returns the number of pairs in the table
 */
template <typename K, typename V>
size_t HashTable<K, V>::size() 
{ return currentSize; }

/**
 * @name Table Size
 * @brief Returns the size of the vector. This is the size of the hash table.
 */
template <typename K, typename V>
size_t HashTable<K, V>::tableSize()
{
	return theLists.size();
}
