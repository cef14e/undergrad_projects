#include <iostream>
#include <string>
#include <string.h>
#include "passserver.h"
#include <termios.h>
#include <unistd.h>

/**
 * The driver program that does not show characters entered by the user when entering
 * a password.
 *
 * Camill Folsom
 * July 18, 2018
 */

using namespace std; 

/**
 * hideKeystrokes and showKeystrokes was referenced from:
 * https://stackoverflow.com/questions/13694170/how-do-i-hide-user-input-with-cin-in-c
 */

/**
 * @name Hide Keystrokes
 * @brief Used to hide the keystrokes while the user enters their password.
 * 		  This is for linux.
 */
void hideKeystrokes()
{
	termios tty;
	
	tcgetattr(STDIN_FILENO, &tty);

	/** Disables the user keystrokes by turning echo off */
	tty.c_lflag &= ~ECHO;

	tcsetattr(STDIN_FILENO, TCSANOW, &tty);
}

/**
 * @name Show Keywstrokes
 * @brief Used to re-enable user keystrokes on linux.
 */
void showKeystrokes()
{
	termios tty;
	
	tcgetattr(STDIN_FILENO, &tty);
	
	/** Re-enables echo so the user can see their keystrokes */
	tty.c_lflag |= ECHO;

	tcsetattr(STDIN_FILENO, TCSANOW, &tty);
}

void Menu()
{
  cout << "\n\n";
  cout << "l - Load From File" << endl;
  cout << "a - Add User" << endl;
  cout << "r - Remove User" << endl;
  cout << "c - Change User Password" << endl;
  cout << "f - Find User" << endl;
  cout << "d - Dump HashTable" << endl;
  cout << "s - HashTable Size" << endl;
  cout << "w - Write to Password File" << endl;
  cout << "x - Exit program" << endl;
  cout << "\nEnter choice : ";
}

int main()
{
	string file, username, newpassword;
	/** Used to pass to load and write_to_file */
	char *filename;
	char in_char;
	int capacity;	
	pair<string, string> user;

	cout << "Enter the prefered hash table capacity: ";

	cin >> capacity;

	PassServer table(capacity);

	cout << table.tableSize() << endl;

	Menu();

	while(std::cin >> in_char)
	{
		/** Makes sure only menu items are entered */
		if(in_char != 'l' && in_char != 'a' && in_char != 'r' && in_char != 'c' &&
		   in_char != 'f' && in_char != 'd' && in_char != 's' && in_char != 'w' && in_char != 'x')
			cout << "*****Error: Invalid entry.  Try again.\n"; 
	
		/** Loads a file into the table */
		if(in_char == 'l')
		{
			cout << "Enter password file name to load from: ";
			cin >> file;
		
			/** Makes filename a copy of the string */
			filename = new char[file.length() + 1];
			strcpy(filename, file.c_str());

			if(!table.load(filename))
				cout << "Error: Cannot open file " << file << endl;
	
			delete [] filename;
		}

		/** Adds a user to the table */
		if(in_char == 'a')
		{
			cout << "Enter username: ";
			cin >> user.first;
			cout << "Enter password: ";
			hideKeystrokes();
			cin >> user.second;
			showKeystrokes();

			if(table.addUser(user))
				cout << "\nUser " << user.first << " added.\n";
			else
				cout << "*****Error: Could not add user " << user.first << endl;
		}

		/** Removes a user from the table */
		if(in_char == 'r')
		{
			cout << "Enter username: ";
			cin >> username;
			if(table.removeUser(username))
				cout <<  "User " << username << " deleted.\n";
			else
				cout << "*****Error: User not found.  Could not delete user.\n";
		}

		/** Changes the users password */
		if(in_char == 'c')
		{
			cout << "Enter username: ";
			cin >> user.first;
			cout << "Enter password: ";
			hideKeystrokes();
			cin >> user.second;

			cout << "\nEnter new password: ";
			cin >> newpassword;
			showKeystrokes();

			if(table.changePassword(user, newpassword))
				cout << "\nPassword changed for user " << user.first << endl;
			else
				cout << "*****Error: Could not change user password.\n";
		}

		/** Checks to see if the user is in the table */
		if(in_char == 'f')
		{
			cout << "Enter username: ";
			cin >> username;
			
			if(table.find(username))
				cout << "\nUser \'" << username << "\' found.\n";
			else
				cout << "\nUser \'" << username << "\' not found.\n";
		}

		/** Displays the table */
		if(in_char == 'd')
			table.dump();

		/** Give the number of elements in the table */
		if(in_char == 's')
			cout << "Size of hashtable: " << table.size();
		
		/** Writes the tables elements to a file */
		if(in_char == 'w')
		{
			cout << "Enter password file name to write to: ";
			cin >> file;

			/** Makes filename a copy of the string */
			filename = new char[file.length() + 1];
			strcpy(filename, file.c_str());

			if(!table.write_to_file(filename))
				cout << "*****Error: Could not write to file.\n";

			delete [] filename;
		}
	
		/** Exits the loop */
		if(in_char == 'x')
			break;

		Menu();
	}

	return 0;
}
