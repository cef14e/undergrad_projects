#ifndef PASSSERVER_H
#define PASSSERVER_H
#include "hashtable.h"
#include <string>

/**
 * Declarations for the class PassServer
 *
 * Camill Folsom
 * July 18, 2018
 */


class PassServer
{
	public:
		PassServer(size_t size = 101);			/** Default Constructor */
		~PassServer();							/** Destructor */
		bool load(const char *filename);		/** Load a file that contains username and password */
		bool addUser(const std::pair<std::string, std::string> & kv); /** Adds a username and password */
		bool addUser(std::pair<std::string, std::string> && kv); /** Move version of addUser */
		bool removeUser(const std::string & k);	/** Delete user k */
		bool changePassword(const std::pair<std::string, std::string> & p, 
							const std::string & newpassword);	/** Change existing users password */
		bool find(const std::string & user);	/** Check if a user exists */
		void dump();							/** Displays the Hash Table */
		size_t size();							/** The number of pair in the table */
		size_t tableSize();						/** The size of the table */
		bool write_to_file(const char *filename); /** Save usernames and passwords to a file */

	private:
		std::string encrypt(const std::string & str);  /** Encrypts str */
		cop4530::HashTable<std::string, std::string> table;
};

#endif
