Hash Tables and Its Applications
================================
This project contained two parts. The first part of the project, required the 
implementation of a hash table class named HashTable. The second part of the 
project, required the development of a simple password server program using the 
hash table developed.

For this project, two versions of the driver program were created. The only difference
between these two programs is the program `proj5.cpp` show the characters the user
enters when entering a password. The program `sproj5.cpp` hides the user entered
characters when they are entering their password. The compiled executables for both 
these programs have been provided.

Compilation
-----------
To compile this program a makefile is provided. The are two version of this program.
The first is made with the 'all' flag and is compiled with the following command:
```
g++ -Wall -std=c++11 passserver.cpp proj5.cpp -lcrypt -o proj5.x
```

This program shows the user entered characters when the user is entering a password. 
The second program can be compiled with the 'sproj' flag. This program is the same as the
one above but it hides the user entered characters when the user is entering their
password.
```
g++ -Wall -std=c++11 passserver.cpp sproj5.cpp -lcrypt -o sproj5.x
```

Note both programs use the GNU C library's **crypt()** function to encrypt passwords.

Usage
-----
To run the program `proj5.x` in Linux the following command can be used:
```
./proj5.x
```

To run the program `sproj5.x` in Linux the following command is used:
```
./sproj5.x
```

The file `test1` contains a series of test commands for the program. These
commands can be piped into either program to test the program. This test file produces
a file called `outtest1` that contains the server's users and their passwords.
Below is the commands to execute either program with the file `test1`:
```
./proj5.x < test1
./sproj5.x < test1
```

Server Menu
-----------
When the program starts it ask the user to define the preferred hash table size.
A server menu is then displayed.

* **l** : Loads usernames and passwords from a file, expects each line to contain 
          a username and password pair
* **a** : Adds a user
* **r** : Remove a user
* **c** : Change a user's password, the password characters appear in `proj5.x` 
          and they do not appear in `sproj5.x` as the user types
* **f** : Prints whether or not the user exits on the server
* **d** : Dumps the HashTable, prints the entire HashTable
* **s** : Prints the size of the HashTable (the number of username/password pairs in the table)
* **w** : Writes the usernames and their passwords to a file that can be loaded
          by this program
* **x** : Exits the program

The file `user_ex` contains example usernames and passwords that can be loaded by
this program.

Compare Key Functions
---------------------
These are functions used by the 'CompareKey' class. These functions are templated
with the template: `template <typename K, typename V>`. Below are the class functions:

* **CompareKey(const K k)** : Constructor, sets the key value
* **operator()(const std::pair<K, V> & kv)** : Compares the key value to the pairs key value

Compare Pair Functions
----------------------
These functions are templated with the template `template <typename K, typename V>`.
Below are the functions for this class.

* **ComparePair(const std::pair<K, V> kv)** :  Sets a key and a value from the key value pair 'kv'
* **operator()(const std::pair<K, V> & kv)** : Compares two pairs, returns true if the key and value
                                               are the same

HashTable Functions
-------------------
These functions are templated with the template `template <typename K, typename V>`.

* **HashTable(size_t size)** : Creates a HashTable, a vector of lists
    * **size** : The size to create the HashTable vector, it uses 'prime_below' to select a size based off of 'size'

* **~HashTable()** : Destructor
* **prime_below (unsigned long n)** : Returns largest prime number <= n or zero if input is too large
* **setPrimes(std::vector<unsigned long>& vprimes)** : Sets all prime number indexes to 1, used by prime_below
* **rehash()** : Called when the number of elements in the table are greater than the vectors size
* **myhash(const K  & k)** : Returns the index to the vector entry where k should be stored
* **contains(const K & k)** : Checks if key k is in the table
* **match(const std::pair<K, V> & kv)** : Checks if the key-value pair is in the table
* **insert(const std::pair<K, V> & kv)** : Adds the key-value pair to the table
    * If kv is already in the table do not add it
    * If the key is in the table but the value is different then it updates the value with the new one
    * Returns **true** if pair is added or value is updated otherwise it returns **false**

* **insert(std::pair<K, V> && kv)** : Move insert function
* **remove(const K & k)** : Deletes the key k and its value from the table
    * Returns **true** only if the pair is deleted

* **clear()** : Deletes all elements in table
* **load(const char \*filename)** : Loads the contents of a file into the table
    * Each line should contain a single key-value pair separated by whitespace

* **write_to_file(const char \*filename)** : Writes the table to a file with the 
                                             same format used for the load function
* **dump()** : Displays the contents of the table, if a entry contains multiple key-value 
               pairs it displays them separated by a colon (:)
* **size()** : Returns the number of pairs in the table
* **tableSize()** : Returns the size of the vector, this is the size of the HashTable


PassServer Functions
--------------------
Below are the functions for the 'PassServer' class.

* **PassServer(size_t size)** : Constructor, creates a HashTable of size
    * The size of the HashTable can be different than the size entered because the
      size of the HashTable is determined by the 'prime_below' function

* **~PassServer()** : Destructor
* **load(const char \*filename)** : Loads a file where each line contains a single 
                                    username and encrypted password separated by a space
* **addUser(const std::pair<std::string, std::string> & kv)** : Adds a username and encrypted password to the HashTable
* **addUser(std::pair<std::string, std::string> && kv)** : Move version of addUser
* **removeUser(const std::string & k)** : Removes the user 'k'
* **changePassword(const std::pair<std::string, std::string> & p, const std::string & newpassword)**:
                Changes the password 'p' to the new password 'newpassword'
* **find(const std::string & user)** : Checks to see if user is in the HashTable
* **dump()** : Displays the table, using the HashTables dump function
* **size()** : Returns the size of the HashTable (the number of pairs in the HashTable)
* **tableSize()** : Returns the size of the HashTable (the length of the vector)
* **write_to_file(const char \*filename)** : Writes the username and encrypted password pair to a file
    * Each line of the file contains one pair and the pair is separated by whitespace

* **encrypt(const std::string & str)** : Encrypt the str parameter and return an encrypted string
